!� ~#�(�!8�+	l	� �~#���\(_{�A8	�[0:�_�� ��~#���L(�U(a_
�(Ի
 ��> 2��2�ABEN
R Echo characters to the terminal

The following special character sequences may be used:
\a     alert (bell)    (ASCII 7)
\b     backspace       (ASCII 8)
\e     escape          (ASCII 27)
\n     line feed       (ASCII 10)
\r     carriage return (ASCII 13)
\l     interpret further chars as lower-case
\u     interpret further chars as upper-case
\\     backslash      ('\')

Note that CP/M converts all your typed characters to upper-case before
providing them to ECHO.COM. Therefore you will need to use \l and \u
to specify the case of characters if it is important (in ESCape sequences,
for example).

Examples:
echo \n\n\rH\lello\u\n
echo \e[2J
$ 