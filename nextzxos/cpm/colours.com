!� �p02�~�!0=�p082��E�0#�R (~#�G "~#�B ��V��V�E�~#���!8��	� ��p0��y0��y0�~�!0��~��#�!8�+~�0?��8�#�S0T4W$SR000TR030hW$Terminal colours utility

Values are specified as standard ZX colours (0..7)

Usage: COLOURS paper ink
   eg: COLOURS 1 6

The currently-selected paper and ink colours can be redefined to any RGB values
by specifying these as 3-digit RGB numbers, 000...777

Usage: COLOURS RGB papervalue inkvalue
       COLOURS RGB 111 740
$