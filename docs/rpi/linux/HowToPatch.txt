How To Patch NextPi2
====================

1) Copy the appropriate update zip file onto the next SD card - for example, if you're updating from 1.91D to 1.92D, you need 191D192D.zip - if there is no unified patch for upgrading between multiple versions (there won't always be) you may need to use multiple zipfiles and update in stages. You can copy the update zip to anywhere that is convenient, we suggest `/home` for this, but you do you :)

You can easily check which version of NextPi you currently via the following commands:
    `.pisend -q`  -- this command detects if you have a pi installed, and configures the speed required, ready for
    `.piver -p` -- this command prints the current version of NextPi.
This should work as long as you have a Next/System version newer than January 2023 - Thus all KS2 machines as shipped with these tools installed as standard.
You can tell if your distro is older than than required, the .piver dotcommand will not exist on your SD card and will give a "No such command" error when you try to run it. If you don't want to update to a bleeding edge GitLab release of the Next/System (and who can blame you?!) you need only add the .piver command to your `/dot` folder - get the latest from https://gitlab.com/thesmog358/tbblue/-/blob/master/dot/PIVER
 
How to copy any needed files to your NextSD card is left as an exercise for the reader, nextsync or a good old fashioned PC with an SD card reader both have been tested with perfect success rates!

2) Navigate to the location of the newly copied update file in the Browser

3) Open the zip by pressing ENTER while it's selected in the Browser window

4) Wait, and marvel that Garry the Great (that's his real name, btw) gave us a native .ZIP extractor on the next.

5) Once extraction is completed succesfully, navigate into the newly created folder, and open the "UpdatePi.bas" file.  As with most firmware updates if you're unlucky with be hit with a powercut at an inoppertune moment recovery can be long-winded and inconvenient, so maybe avoid doing if you're running your next on batteries or during a thunderstorm! ;-)

6) Follow the onscreen prompts, making sure not to turn off your next once the actual update has begun.

Once finished the script will return, telling you that it has completed - ending with a graceful "STOP" statement. You can now delete the newly created directory and extracted files plus the original update .zip file, if you want.

Good luck, fingers crossed, and Happy NextPiing. 
