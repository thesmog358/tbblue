What's new in NextZXOS v2.09
============================
This document describes the main new features in NextZXOS v2.09. It is
mainly intended for those people who have previously been using v2.08
and want to know what's changed.

If you are new to NextZXOS and NextBASIC, all the information in this
document is also integrated into the main documentation files:

NextBASIC_File_Commands.pdf
NextBASIC_New_Commands_and_Features.pdf
NextZXOS_Editor_Features.pdf
NextZXOS_and_esxDOS_APIs.pdf


Editing commands
================
The following new commands (and command variants) are available:

LINE NEW start,step
-------------------
Starts automatic line numbering beginning with line number start and
incrementing numbers by step. If start is omitted, lines start after the
current editor line. If step is omitted, numbers increment by 10.

LINE n,m TO start,step
----------------------
Renumbers a block of lines within the BASIC program. All lines between n
and m (inclusive) are renumbered (and, if necessary, moved), with new
starting line number start and incrementing numbers by step.

BANK n LINE x,y REM
-------------------
This new variation of the BANK n LINE x,y command strips out all
comments and blank lines when copying lines to the bank.


Code options
============
A new bit has been added to the %CODE pseudo-variable:

Bit:    Feature:
----    --------
2       Set to allow editor entry of legacy DEF FN statements that access
        the DEFADD system variable (disables editor entry of DEF FN
        statements using new parameter-passing features). New and legacy
        DEF FNs may be mixed in the same program - this bit only affects
        how statements entered in the editor (or .txt2bas) are treated.


File system options
===================
A new bit has been added to the %FORMAT pseudo-variable:

Bit     Behaviour
---     ---------
2       If set, disables writes to the metadata cache (c:/nextzxos/metadata)


Miscellaneous new features
==========================
-   A new Autonumber option is available in the NextBASIC editor,
    starting automatic line numbering at the current line in steps
    of 10.
-   Experimental support for double-sided .DSK files has been enabled
    (eg Sword of Ianna, ianna-3dos.dsk).
-   Buttons may be skipped by pressing BREAK during keyjoy configuration
    in the TAP/TZX/SNA/Z80/tape loaders.


System variable changes
=======================
REGNUOY+9 (5B33) is replaced with:
X1 5B33 23347 CACHEBNK 8K bank id holding cached program data.

TARGET (5B58) is replaced with:
X2 5B58 23384 RETVARS Address of local variables on return stack.

DEFADD (5C0B) is restored, and used when a legacy 48K BASIC DEF FN is
evaluated.


Sprite engine data
==================

Data for the sprite engine is stored in 16K RAM bank 8. Access to this bank is restricted under BASIC (due to the presence of NextZXOS system data). However, the offset of the sprite engine data is provided (from NextZXOS v2.09) for drivers or other system extensions to make use of if desired.

RAM 8 offset:
$1ffe (2)			uvec8_sprite_data: offset of sprite engine data in RAM 8
uvec8_sprite_data:
+0 (128*16)			sprite0_data..sprite127_data
+$800 (32)			sprite dirty/automove flags

spriteN_data:		16-byte structure for each of sprites 0..127
                                   bit: 7  6  5  4  3  2  1  0
+0 (1)			sprite_attrib0: X7 X6 X5 X4 X3 X2 X1 X0
+1 (1)			sprite_attrib1: Y7 Y6 Y5 Y4 Y3 Y2 Y1 Y0
+2 (1)			sprite_attrib2: P3 P2 P1 P0 XM YM RF X8
+3 (1)			sprite_attrib3: V  @R N5 N4 N3 N2 N1 N0
+4 (1)			sprite_attrib4: @a @b @c XS XS YS YS @M
+5 (1)			sprite_flags:   XR YR IS IB XM YM B1 B0
+6 (1)			sprite_xMin: min X coord
+7 (1)			sprite_xMax: max X coord
+8 (1)			sprite_iMin: min image (bits 6..1), xMin8 (bit 0),
                                     update image even if not moving (bit 7)
+9 (1)			sprite_iMax: max image (bits 6..1), xMax8 (bit 0),
                                     decrement flag (bit 7)
+10 (1)			sprite_xStep: X step (signed)
+11 (1)			sprite_yMin: min Y coord
+12 (1)			sprite_yMax: max Y coord
+13 (1)			sprite_yStep: Y step (signed)
+14 (1)			sprite_rate: 0=every MOVE, 1=wait 1 MOVE etc
+15 (1)			sprite_delay: # MOVEs left to wait

NOTE: sprite_attrib bytes as defined by the corresponding Next registers ($35..$39), with the following changes:
				@R=anchor (0)/relative (1)
				@a=palette relative flag
				@b=pattern relative flag
				@c=composite/unified flag
				@M=mirror behaviour:
					0=use auto-flip x-mirror and y-mirror behaviour
					1=generate directional x-mirror/y-mirror/rotate

sprite_flags bits:
	XR=X running flag
	YR=Y running flag
	IS=image stop: turn off sprite if reach end
      IB=image behaviour: 0=cycle, 1=bounce
      XM=auto-flip x-mirror
      YM=auto-flip y-mirror
      B1..B0=behaviour:
        		00=reflect this dirn
			01=stop, restart other dirn
			10=stop this dirn
			11=disable, make invisible

sprite dirty/automove flags, arranged in pairs of bytes:
+0	dirty flags (sprite data changed)
+1	automove flags

The first pair of bytes corresponds to sprite 0 (bit 7)..sprite 7 (bit 0)
The second pair of bytes corresponds to sprite 8 (bit 7)..sprite 15 (bit 0) etc

