/home is a place for you to keep your own files you create, files in here
will never be replaced, overwritten, or deleted by /NextZXOS upgrades and some
apps may offer to save files in this location by default, to help with keeping
your SD card neat and organised
